#!/usr/bin/env bash

################################################################################
# Dépôt Git :
# https://framagit.org/zorval/scripts/check-nrpe
################################################################################

# Copyright © 2019-2020 Alban Vidal - zordhak@debian.org

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

################################################################################

REPO_LIST_FILE=/etc/zorval/git-monitored-repos
RC=0

if [ ! -f $REPO_LIST_FILE ] ; then
    echo "[CRITICAL] Le fichier $REPO_LIST_FILE n'existe pas !"
    exit 2
fi


while read GIT_REPO; do

    if [ ! -d "$GIT_REPO" ] ; then
        OUTPUT+="\nDépôt $GIT_REPO : le répertoire n'existe pas !"
        RC=2
        continue
    fi

    cd "$GIT_REPO"

    # Une fois sur 10, lancement de « git gc » qui permet d'optimiser les dépôt locaux
    if [ $(( $RANDOM % 10 )) -eq 0 ]; then
        git gc
    fi

    # Options du « git pull » :
    # --quiet : pull silencieux
    # --no-commit : évite les autocommit qui lancent des merges requests locales
    # --prune : Permet de supprimer les références locales à des branches distantes qui n'existent plus
    if ! git pull --quiet --no-commit --prune ; then
        OUTPUT+="\nDépôt $GIT_REPO : erreur détectée lors de la récupération du dépôt distant !"
        [ $RC -eq 2 ] || RC=1
    else
        OUTPUT+="\nDépôt $GIT_REPO : OK"
    fi

done < $REPO_LIST_FILE


if [ $RC -eq 0 ]; then
    echo -n "OK"
elif [ $RC -eq 1 ]; then
    echo -n "[WARNING]"
else
    echo -n "[CRITICAL]"
    RC=2
fi

echo -e "$OUTPUT"
exit $RC
