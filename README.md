# Check NRPE

Boite à outils de plusieurs script NRPE

## Paquets à installer

+ nagios-nrpe-server
+ needrestart
+ bc
+ sudo

```bash
apt install            \
    nagios-nrpe-server \
    needrestart        \
    bc                 \
    sudo
```

## Pour appliquer la conf

```bash
command cp -dur /srv/git/conf/conf-10-check-nrpe/conf/common/* /

systemctl restart nagios-nrpe-server.service
```
